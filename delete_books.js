const { Book } = require('./models')
const books = require('./models/books')

const query = {
    where: {
        id: 1
    }
}

Book.destroy(query)
.then(books => {
    console.log(books)
})
.catch(err => {
    console.log(err)
})