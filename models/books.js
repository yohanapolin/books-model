'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Book extends Model {
        static associate(models){

        }
    };
    Book.init({
       Id : {
           type: DataTypes.INTEGER, 
           primaryKey: true,
           autoIncrement: true,
       },
       Isbn : {
           type : DataTypes.STRING, 
       },
       Judul : {
           type : DataTypes.STRING,

       },
       Sinopsi : {
           type : DataTypes.STRING
       },
       Penulis : {
           type : DataTypes.STRING,
       },
       genre : {
            type : DataTypes.STRING
       }

    }, {sequelize,
        modelName: 'Book',
    });
    return Book;
}
